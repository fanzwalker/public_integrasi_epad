<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class HistoryPerson extends Model
{
  protected $fillable = ['first_name','last_name','email'];
}
