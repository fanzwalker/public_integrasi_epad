<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\MATANGD as DbPen;
use App\models\InStage\MATANGD as DbStages;
class MATANGDController extends Controller
{
    public function index(){
        return view('main.MATANGD.index');
    }
    public function getFrom() {
      $DbPend = $this->GetFromDBPendapatan();
      if(count($DbPend) >  0 ):
        if($this->storeToStages($DbPend)):
          $DbStages = DbStages::all();
          if(count($DbStages) > 0 ):
            echo json_encode($DbStages);
          endif;
        endif;
      endif;
    }
    public function GetFromDBPendapatan() {
      $DbPend = DbPen::all()->toArray();
      if(count($DbPend) >  0 ):
        return $DbPend;
      else:
        return [];
      endif;
    }
    public function storeToStages($data){
      DbStages::truncate();
      foreach ($data as $d):
        DbStages::insert($d);
      endforeach;
      return true;
    }
}
