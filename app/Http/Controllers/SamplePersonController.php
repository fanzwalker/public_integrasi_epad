<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\SamplePerson;
use Illuminate\Support\Facades\DB;

class SamplePersonController extends Controller
{
    public function index(){
      $person = DB::connection('tes1')->table('SamplePerson')->get();
      return view('main.person.index', ['person' => $person]);
    }
}
