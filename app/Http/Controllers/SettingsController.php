<?php

namespace Integrasi\Http\Controllers;

use Illuminate\Http\Request;
use Integrasi\libs\GlobalQuery;
class SettingsController extends Controller
{
    public $response;
    public function ResetTableLog(Request $request) {
      $tables = $request->get('logTable');
      if(GlobalQuery::ResetLogTable($tables)):
        $this->response = [
          'status'  => true,
          'info'    => 'tabel log berhasil direset / dihapus'
        ];
      else:
        $this->response = [
          'status'  => false,
          'info'    => 'table log gagal direset'
        ];
      endif;
      return response($this->response);
    }
}