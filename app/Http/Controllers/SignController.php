<?php

namespace Integrasi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Auth;
class SignController extends Controller
{
  public function index() {
    $dblist = DB::select("EXEC sp_databases");
    return view('welcome',['listDB' => $dblist]);
  }
  public function signIn(Request $request) {
    $field = filter_var($request->input('email_or_username'), FILTER_VALIDATE_EMAIL) ? 'email' : 'username';
    $request->merge([$field => $request->input('email_or_username')]);
    if (Auth::attempt($request->only($field, 'password')))
    {
      session(['tahun' => $request->input('tahun')]);
      return redirect()->route('home');;
    }
    return redirect()->back()->with('failed', 'Login gagal');
  }
}
