<?php

namespace Integrasi\Http\Controllers;

use Integrasi\libs\ifHelper as IfHelper;;
use Integrasi\libs\SkpQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class SuratKetetapanPajakController extends Controller
{
  public $response;
  public function index(){
    return view('main.SKP.index');
  }
  public function getFromPendapatan(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $get = DB::connection('validmd')->select(SkpQuery::GetSkp($fromTgl, $toTgl));
    if(count($get) > 0 ):
      $this->dropOnStage($fromTgl, $toTgl);
      if($this->storeToStage($get)):
        $this->response = ['status' => true, 'info' => 'berhasil simpan ke stage'];
        return response($this->response);
      else:
        $this->response = ['status' => false, 'info' => 'gagal simpan ke stage'];
        return response($this->response);
      endif;
    else:
      $this->response = ['status' => false, 'info' => 'Data Kosong'];
      return response($this->response);
    endif;
  }
  public function dropOnStage($fromTgl, $toTgl, $noSkp = []){
    if(count($noSkp) > 0):
      return DB::table('TempSKP')
        ->whereBetween('TGLSKP', [$fromTgl, $toTgl])
        ->whereIn('NOSKP', $noSkp)
        ->delete();
    else:
      return DB::table('TempSKP')->whereBetween('TGLSKP', [$fromTgl, $toTgl])->delete();
    endif;
  }
  public function storeToStage($data){
    foreach ($data as $d):
      $prepareData = [
        'NOSKP'       => $d->NOSKP,
        'UNITKEY'     => $d->UNITKEY,
        'NPWPD'       => $d->NPWPD,
        'TGLSKP'      => $d->TGLSKP,
        'PENYETOR'    => $d->PENYETOR,
        'ALAMAT'      => $d->ALAMAT,
        'URAISKP'     => $d->URAISKP,
        'TGLTEMPO'    => $d->TGLTEMPO,
        'BUNGA'       => $d->BUNGA,
        'KENAIKAN'    => $d->KENAIKAN,
        'STATUS'      => $d->STATUS,
        'TGLVALID'    => $d->TGLVALID,
        'KODE_REK'    => $d->KODE_REK,
        'REKENING'    => $d->REKENING,
        'NILAI'       => $d->NILAI
      ];
      DB::table('TempSKP')->insert($prepareData);
    endforeach;
    return true;
  }
  public function getFromStage(Request $request){
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $tempSKP = DB::table('TempSKP')
      ->whereBetween('TGLSKP', [$fromTgl, $toTgl])
      ->orderBy('TGLSKP', 'asc')
      ->get();
    $logSKP = DB::table('LogSKP')
      ->whereBetween('TGLSKP', [$fromTgl, $toTgl])
      ->orderBy('TGLSKP', 'asc')
      ->get();
    $dataReturn = [
      'TempSKP' => $tempSKP,
      'LogSKP'  => $logSKP
    ];
    return $dataReturn;
  }
  public function storeSKPSikpd(Request $request){
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $noSKP = $request->get('noSKP');
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($noSKP) > 0):
      $data =  DB::select(SkpQuery::GetTempSkpForSIKPD($fromTgl, $toTgl, $noSKP));
      if(count($data) > 0):
        foreach ($data as $d):
          $prepareData = [
            'UNITKEY'     =>  $d->UNITKEY,
            'NOSKP'       =>  $d->NOSKP,
            'KDSTATUS'    =>  $d->STATUS,
            'KEYBEND'    =>  $d->KEYBEND,
            'NPWPD'     =>  $d->NPWPD,
            'IDXKODE'    =>  $d->IDXKODE,
            'TGLSKP'      =>  $d->TGLSKP,
            'PENYETOR'    =>  $d->PENYETOR,
            'ALAMAT'      =>  $d->ALAMAT,
            'URAISKP'      =>  $d->URAISKP,
            'TGLTEMPO'    =>  $d->TGLTEMPO,
            'BUNGA'    =>  $d->BUNGA,
            'KENAIKAN'    =>  $d->KENAIKAN,
            'TGLVALID'    =>  $d->TGLVALID
          ];
          DB::connection($sipkd)->table('SKP')->insert($prepareData);
        endforeach;
        if($this->insertToSKPDET($fromTgl, $toTgl, $noSKP)):
          $this->storeToLogSTS($fromTgl, $toTgl, $noSKP);
          $this->dropOnStage($fromTgl, $toTgl, $noSKP);
          $this->response = [
            'info'    => 'Insert Data Berhasil',
            'status'  =>  'success',
          ];
        else:
          $this->response = [
            'info'    => 'Insert Data SKPDET Gagal, Kemungkinan data kosong / duplikat data',
            'status'  =>  'failed',
          ];
        endif;
      else:
        $this->response = [
          'info'    => 'Data Tidak Ada',
          'status'  =>  'failed',
        ];
      endif;
    else:
      $this->response = [
        'info'    => 'Data tidak dipilih',
        'status'  =>  'error',
      ];
    endif;
    return response($this->response);
  }
  public function insertToSKPDET($fromTgl, $toTgl, $noSKP) {
    $data = DB::select(SkpQuery::GetTempSkpForSKPDET($fromTgl, $toTgl, $noSKP));
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($data) > 0 ):
      foreach ($data as $d):
        $prepareData = [
          'MTGKEY'    =>  $d->MTGKEY,
          'NOJETRA'   =>  $d->NOJETRA,
          'UNITKEY'   =>  $d->UNITKEY,
          'NOSKP'     =>  $d->NOSKP,
          'NILAI'     =>  $d->NILAI
        ];
        DB::connection($sipkd)->table('SKPDET')->insert($prepareData);
      endforeach;
      return true;
    else:
      return true;
    endif;
  }
  public function storeToLogSTS($from_tgl, $to_tgl , $noSkp){
    $data = DB::table('TempSKP')
      ->whereBetween('TGLSKP', [$from_tgl, $to_tgl])
      ->whereIn('NOSKP', $noSkp)
      ->orderBy('TGLSKP', 'asc')
      ->get();
    foreach ($data as $d):
      $prepareData = [
        'NOSKP'       => $d->NOSKP,
        'UNITKEY'     => $d->UNITKEY,
        'NPWPD'       => $d->NPWPD,
        'TGLSKP'      => $d->TGLSKP,
        'PENYETOR'    => $d->PENYETOR,
        'ALAMAT'      => $d->ALAMAT,
        'URAISKP'     => $d->URAISKP,
        'TGLTEMPO'    => $d->TGLTEMPO,
        'BUNGA'       => $d->BUNGA,
        'KENAIKAN'    => $d->KENAIKAN,
        'STATUS'      => $d->STATUS,
        'TGLVALID'    => $d->TGLVALID,
        'KODE_REK'    => $d->KODE_REK,
        'REKENING'    => $d->REKENING,
        'NILAI'       => $d->NILAI
      ];
      DB::table('LogSKP')->insert($prepareData);
    endforeach;
    return true;
  }
}
