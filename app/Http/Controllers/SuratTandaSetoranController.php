<?php

namespace Integrasi\Http\Controllers;

use Integrasi\libs\StsQuery;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Integrasi\libs\ifHelper as IfHelper;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class SuratTandaSetoranController extends Controller
{
  public $response;
  public function index() {
    return view('main.STS.index');
  }
  public function getFromPendapatan(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $get = DB::connection('validmd')->select(StsQuery::GetSts($fromTgl, $toTgl));
    if(count($get) > 0 ):
      $this->dropOnStage($fromTgl, $toTgl);
      if($this->storeToStage($get)):
        $this->response = ['status' => true, 'info' => 'berhasil simpan ke stage'];
        return response($this->response);
      else:
        $this->response = ['status' => false, 'info' => 'gagal simpan ke stage'];
        return response($this->response);
      endif;
    else:
      $this->response = ['status' => false, 'info' => 'Data Kosong'];
      return response($this->response);
    endif;
  }
  public function dropOnStage($fromTgl, $toTgl, $noSts = []){
    if(count($noSts) > 0):
      return DB::table('TempSTS')
        ->whereBetween('TglSetoran', [$fromTgl, $toTgl])
        ->whereIn('NoSTS', $noSts)
        ->delete();
    else:
     return DB::table('TempSTS')->whereBetween('TglSetoran', [$fromTgl, $toTgl])->delete();
    endif;
  }
  public function storeToStage($data){
    foreach ($data as $d):
      $prepareData = [
        'NoSTS'         => $d->NoSTS,
        'TglSetoran'    => $d->TglSetoran,
        'KodeUPT'       => $d->KodeUPT,
        'UPT'           => $d->UPT,
        'KodeRekening'  => $d->KodeRekening,
        'Rekening'      => $d->Rekening,
        'NIP'           => $d->NIP,
        'Bendahara'     => $d->Bendahara,
        'Keterangan'    => $d->Keterangan,
        'JmlSetoran'    => $d->JmlSetoran
      ];
      DB::table('TempSTS')->insert($prepareData);
    endforeach;
    return true;
  }
  public function getFromStage(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $tempSTS = DB::table('TempSTS')
      ->whereBetween('TglSetoran', [$fromTgl, $toTgl])
      ->orderBy('TglSetoran', 'asc')
      ->get();
    $logSTS = DB::table('LogSTS')
      ->whereBetween('TglSetoran', [$fromTgl, $toTgl])
      ->orderBy('TglSetoran', 'asc')
      ->get();
    $dataReturn = [
      'TempSTS' => $tempSTS,
      'LogSTS'  => $logSTS
    ];
    return $dataReturn;
  }
  public function storeSTSSikpd(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $noSTS = $request->get('noSTS');
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($noSTS) > 0 ):
      $data =  DB::select(StsQuery::GetTempStsForSikpd($fromTgl, $toTgl, $noSTS));
      if(count($data) > 0):
        foreach ($data as $d):
          $prepareData = [
            'UNITKEY'     =>  $d->UNITKEY,
            'NOSTS'       =>  $d->NoSTS,
            'KEYBEND1'    =>  $d->KEYBEND1,
            'KDSTATUS'    =>  $d->KDSTATUS,
            'IDXKODE'     =>  $d->IDXKODE,
            'KEYBEND2'    =>  $d->KEYBEND2,
            'IDXTTD'      =>  $d->IDXTTD,
            'NOBBANTU'    =>  $d->NOBBANTU,
            'TGLSTS'      =>  $d->TGLSTS,
            'URAIAN'      =>  $d->Keterangan,
            'TGLVALID'    =>  $d->TGLVALID
          ];
          DB::connection($sipkd)->table('STS')->insert($prepareData);
        endforeach;
        if($this->insertToRKMDETD($fromTgl, $toTgl, $noSTS)):
          $this->insertSTSTBP($fromTgl, $toTgl, $noSTS);
          $this->storeToLogSTS($fromTgl, $toTgl, $noSTS);
          $this->dropOnStage($fromTgl, $toTgl, $noSTS);
          $this->response = [
            'info'    => 'Insert Data Berhasil',
            'status'  =>  'success',
          ];
        else:
          $this->response = [
            'info'    => 'Insert Data RKMDETD Gagal, Kemungkinan data kosong / duplikat data',
            'status'  =>  'error',
          ];
        endif;
      else:
        $this->response = [
          'info'    => 'Data Tidak Ada',
          'status'  =>  'error',
        ];
      endif;
    else:
      $this->response = [
        'info'    => 'Data tidak dipilih',
        'status'  =>  'error',
      ];
    endif;
    return response($this->response);
  }
  public function insertToRKMDETD($fromTgl, $toTgl, $noSts){
    $data = DB::select(StsQuery::GetTempStsForRKMDETD($fromTgl, $toTgl, $noSts));
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($data) > 0 ):
      foreach ($data as $d):
        $prepareData = [
          'MTGKEY'    =>  $d->MTGKEY,
          'UNITKEY'   =>  $d->UNITKEY,
          'NOSTS'     =>  $d->NOSTS,
          'NOJETRA'   =>  $d->NOJETRA,
          'NILAI'     =>  $d->NILAI
        ];
        DB::connection($sipkd)->table('RKMDETD')->insert($prepareData);
      endforeach;
      return true;
    else:
      return true;
    endif;
  }
  public function insertSTSTBP($fromTgl, $toTgl, $noTBP) {
    $data = DB::select(StsQuery::getTempForSTSTBP($fromTgl, $toTgl, $noTBP));
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($data) > 0):
      foreach ($data as $d):
        $prepareData = [
          'NOTBP'     =>  $d->NOTBP,
          'UNITKEY'   =>  $d->UNITKEY,
          'NOSTS'     =>  $d->NoSTS
        ];
        DB::connection($sipkd)->table('TBPSTS')->insert($prepareData);
      endforeach;
    endif;
  }
  public function storeToLogSTS($from_tgl, $to_tgl , $noSts){
    $data = DB::table('TempSTS')
      ->whereBetween('TglSetoran', [$from_tgl, $to_tgl])
      ->whereIn('NoSTS', $noSts)
      ->orderBy('TglSetoran', 'asc')
      ->get();
    foreach ($data as $d):
      $prepareData = [
        'NoSTS'         => $d->NoSTS,
        'TglSetoran'    => $d->TglSetoran,
        'KodeUPT'       => $d->KodeUPT,
        'UPT'           => $d->UPT,
        'KodeRekening'  => $d->KodeRekening,
        'Rekening'      => $d->Rekening,
        'NIP'           => $d->NIP,
        'Bendahara'     => $d->Bendahara,
        'Keterangan'    => $d->Keterangan,
        'JmlSetoran'    => $d->JmlSetoran
      ];
      DB::table('LogSTS')->insert($prepareData);
    endforeach;
    return true;
  }
//  public function FormatRupiah($angka,$use_Rp)//example FormatRupiah(12345,true);
//  {
//    if($use_Rp === false):
//      return number_format($angka,2,',','.');
//    else:
//      return "Rp. " .number_format($angka,2,',','.');
//    endif;
//  }
}
