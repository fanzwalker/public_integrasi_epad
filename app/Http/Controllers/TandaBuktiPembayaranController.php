<?php

namespace Integrasi\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Integrasi\libs\TbpQuery;
use Integrasi\libs\ifHelper as IfHelper;
use Illuminate\Support\Facades\Session;
use Illuminate\Support\Facades\Config;

class TandaBuktiPembayaranController extends Controller
{
  public $response;
  public function index() {
    return view('main.TBP.index');
  }
  public function getFromPendapatan(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $get = DB::connection('validmd')->select(TbpQuery::GetTbp($fromTgl, $toTgl));
    if(count($get) > 0 ):
      $this->dropOnStage($fromTgl, $toTgl);
      if($this->storeToStage($get)):
        $this->response = ['status' => true, 'info' => 'berhasil simpan ke stage'];
        return response($this->response);
      else:
        $this->response = ['status' => false, 'info' => 'gagal simpan ke stage'];
        return response($this->response);
      endif;
    else:
      $this->response = ['status' => false, 'info' => 'Data Kosong'];
      return response($this->response);
    endif;
  }
  public function dropOnStage($fromTgl, $toTgl, $noTbp = []){
    if(count($noTbp) > 0):
      return DB::table('TempTBP')
        ->whereBetween('TGLTBP', [$fromTgl, $toTgl])
        ->whereIn('NOTBP', $noTbp)
        ->delete();
    else:
      return DB::table('TempTBP')->whereBetween('TGLTBP', [$fromTgl, $toTgl])->delete();
    endif;
  }
  public function storeToStage($data){
    foreach ($data as $d):
      $prepareData = [
        'NOTBP'       => $d->NOTBP,
        'NOSKP'       => $d->NOSKP,
        'UNITKEY'     => $d->UNITKEY,
        'NPWPD'       => $d->NPWPD,
        'TGLTBP'      => $d->TGLTBP,
        'PENYETOR'    => $d->PENYETOR,
        'ALAMAT'      => $d->ALAMAT,
        'URAITBP'     => $d->URAITBP,
        'BUNGA'       => $d->BUNGA,
        'KENAIKAN'    => $d->KENAIKAN,
        'TGLVALID'    => $d->TGLVALID,
        'KODE_REK'    => $d->KODE_REK,
        'REKENING'    => $d->REKENING,
        'NILAI'       => $d->NILAI
      ];
      DB::table('TempTBP')->insert($prepareData);
    endforeach;
    return true;
  }
  public function getFromStage(Request $request) {
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $tempTBP = DB::table('TempTBP')
      ->whereBetween('TGLTBP', [$fromTgl, $toTgl])
      ->orderBy('TGLTBP', 'asc')
      ->get();
    $logTBP = DB::table('LogTBP')
      ->whereBetween('TGLTBP', [$fromTgl, $toTgl])
      ->orderBy('TGLTBP', 'asc')
      ->get();
    $dataReturn = [
      'TempTBP' => $tempTBP,
      'LogTBP'  => $logTBP
    ];
    return $dataReturn;
  }
  public function storeTBPSikpd(Request $request){
    $fromTgl = IfHelper::splitTgl($request->get('fromTgl'));
    $toTgl = IfHelper::splitTgl($request->get('toTgl'));
    $noTBP = $request->get('noTBP');
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($noTBP) > 0):
      $data =  DB::select(TbpQuery::GetTempTbpForSIKPD($fromTgl, $toTgl, $noTBP));
      if(count($data) > 0):
        foreach ($data as $d):
          $prepareData = [
            'UNITKEY'     =>  $d->UNITKEY,
            'NOTBP'       =>  $d->NOTBP,
            'KEYBEND1'    =>  $d->KEYBEND1,
            'KDSTATUS'    =>  $d->KDSTATUS,
            'KEYBEND2'    =>  $d->KEYBEND2,
            'IDXKODE'     =>  $d->IDXKODE,
            'TGLTBP'      =>  $d->TGLTBP,
            'PENYETOR'    =>  $d->PENYETOR,
            'ALAMAT'      =>  $d->ALAMAT,
            'URAITBP'     =>  $d->URAITBP,
            'TGLVALID'    =>  $d->TGLVALID
          ];
          DB::connection($sipkd)->table('TBP')->insert($prepareData);
        endforeach;
        if($this->insertToSKPDET($fromTgl, $toTgl, $noTBP)):
          $this->insertSKPTBP($fromTgl, $toTgl, $noTBP);
          $this->storeToLogTBP($fromTgl, $toTgl, $noTBP);
          $this->dropOnStage($fromTgl, $toTgl, $noTBP);
          $this->response = [
            'info'    => 'Insert Data Berhasil',
            'status'  =>  'success',
          ];
        else:
          $this->response = [
            'info'    => 'Insert Data TBPDET Gagal, Kemungkinan data kosong / duplikat data',
            'status'  =>  'failed',
          ];
        endif;
      else:
        $this->response = [
          'info'    => 'Data Tidak Ada',
          'status'  =>  'failed',
        ];
      endif;
    else:
      $this->response = [
        'info'    => 'Data tidak dipilih',
        'status'  =>  'error',
      ];
    endif;
    return response($this->response);
  }
  public function insertToSKPDET($fromTgl, $toTgl, $noSKP) {
    $data = DB::select(TbpQuery::GetTempTbpForTBPDET($fromTgl, $toTgl, $noSKP));
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($data) > 0 ):
      foreach ($data as $d):
        $prepareData = [
          'MTGKEY'    =>  $d->MTGKEY,
          'NOJETRA'   =>  $d->NOJETRA,
          'UNITKEY'   =>  $d->UNITKEY,
          'NOTBP'     =>  $d->NOTBP,
          'NILAI'     =>  $d->NILAI
        ];
        DB::connection($sipkd)->table('TBPDETD')->insert($prepareData);
      endforeach;
      return true;
    else:
      return true;
    endif;
  }
  public function insertSKPTBP($fromTgl, $toTgl, $noTBP) {
    $data = DB::select(TbpQuery::getTempForSKPTBP($fromTgl, $toTgl, $noTBP));
    $tahun = Session::get('tahun');
    $sipkd = IfHelper::SipkdName($tahun);
    Config::set('database.connections.'.$sipkd, array(
      'driver' => 'sqlsrv',
      'host' => 'DESKTOP-624EQDP',
      'port' => '',
      'database' => $sipkd,
      'username' => 'sa',
      'password' => 'valid!',
      'charset' => 'utf8',
      'prefix' => '',
    ));
    if(count($data) > 0):
      foreach ($data as $d):
        $prepareData = [
          'NOTBP'     =>  $d->NOTBP,
          'UNITKEY'   =>  $d->UNITKEY,
          'NOSKP'     =>  $d->NOSKP
        ];
        DB::connection($sipkd)->table('SKPTBP')->insert($prepareData);
      endforeach;
    endif;
  }
  public function storeToLogTBP($from_tgl, $to_tgl , $noTbp){
    $data = DB::table('TempTBP')
      ->whereBetween('TGLTBP', [$from_tgl, $to_tgl])
      ->whereIn('NOTBP', $noTbp)
      ->orderBy('TGLTBP', 'asc')
      ->get();
    foreach ($data as $d):
      $prepareData = [
        'NOTBP'       => $d->NOTBP,
        'NOSKP'       => $d->NOSKP,
        'UNITKEY'     => $d->UNITKEY,
        'NPWPD'       => $d->NPWPD,
        'TGLTBP'      => $d->TGLTBP,
        'PENYETOR'    => $d->PENYETOR,
        'ALAMAT'      => $d->ALAMAT,
        'URAITBP'     => $d->URAITBP,
        'BUNGA'       => $d->BUNGA,
        'KENAIKAN'    => $d->KENAIKAN,
        'TGLVALID'    => $d->TGLVALID,
        'KODE_REK'    => $d->KODE_REK,
        'REKENING'    => $d->REKENING,
        'NILAI'       => $d->NILAI
      ];
      DB::table('LogTBP')->insert($prepareData);
    endforeach;
    return true;
  }
}
