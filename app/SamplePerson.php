<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class SamplePerson extends Model
{
    protected $fillable = ['first_name','last_name','email'];
}
