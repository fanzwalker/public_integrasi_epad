<?php
/**
 * Created by PhpStorm.
 * User: IF-PC
 * Date: 12/9/2017
 * Time: 12:39 PM
 */

namespace Integrasi\libs;


class GlobalQuery
{
  public static function ResetLogTable(array $table = []) {
    $query = "";
    if(count($table) > 0 ):
      for($i = 0; $i < count($table); $i++):
        if($i === count($table)-1):
          $query .= "delete from ".$table[$i]." ";
        endif;
      endfor;
    endif;
    return $query;
  }
}