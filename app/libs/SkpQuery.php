<?php

namespace Integrasi\libs;

class SkpQuery{
  public static function GetSkp($fromTgl, $toTgl) {
    return "SELECT RTRIM(hd.KohirID) NOSKP,
              UNITKEY=ISNULL((SELECT RTRIM(p.CONFIGVAL) FROM PEMDA p WHERE p.CONFIGID='cur_uskpkd'),''), 
              mp.ObyekBadanNo NPWPD, 
              hd.TglPenetapan TGLSKP,
              mp.NamaPemilik PENYETOR, 
              mp.AlamatBadan ALAMAT, 
              dt.JudulReklame URAISKP,
              hd.TglJatuhTempo TGLTEMPO,
              CAST(0 AS MONEY) BUNGA,
              CAST(0 AS MONEY) KENAIKAN,
              [STATUS]=(CASE WHEN IsPaid = 'Y' THEN 'Lunas' ELSE (CASE WHEN TglJatuhTempo >= getdate() THEN '' ELSE 'Jatuh Tempo' END) END),
              hd.TglJatuhTempo TGLVALID,
              mj.KodeRekening KODE_REK,
              mj.NamaJenisPendapatan REKENING,
              dt.TarifPajak NILAI
              FROM PenetapanReklameHdr hd 
              LEFT OUTER JOIN PenetapanReklameDtl dt ON hd.NoKohir=dt.NoKohir
              LEFT OUTER JOIN MsWP ms ON hd.ObyekBadanID=ms.ObyekBadanID
              LEFT OUTER JOIN MsWPData mp ON ms.WPID=mp.WPID
              LEFT OUTER JOIN MsJenisPendapatan mj ON ms.UsahaBadan=mj.JenisPendapatanID
              WHERE hd.TglPenetapan BETWEEN '".$fromTgl."' AND '".$toTgl."'
            
            UNION
            SELECT RTRIM(KohirID),
              UNITKEY=ISNULL((SELECT RTRIM(p.CONFIGVAL) FROM PEMDA p WHERE p.CONFIGID='cur_uskpkd'),''),
              p.ObyekBadanNo NPWPD,
              p.TglPenetapan TGLSKP,
              p.NamaPemilik PENYETOR,
              p.AlamatBadan ALAMAT,
              p.Jenis URAISKP,
              p.TglJatuhTempo TGLTEMPO,
              BUNGA=ISNULL((CASE WHEN Pajak=0 THEN 0 ELSE (JDenda/Pajak) END),0),
              JDenda KENAIKAN,
              [STATUS]=(CASE WHEN IsPaid = 'Y' THEN 'Lunas' ELSE (CASE WHEN TglJatuhTempo >= getdate() THEN '' ELSE 'Jatuh Tempo' END) END),
              p.TglJatuhTempo TGLVALID,
              p.KodeRekening KODE_REK,
              p.Jenis REKENING,
              p.Pajak NILAI
              FROM vw_penetapanomzet p
              WHERE p.TglPenetapan BETWEEN '".$fromTgl."' AND '".$toTgl."'            
";
  }

  public static function GetTempSkpForSIKPD($fromTgl, $toTgl, $noSkp) {
    $skpParam = self::stsParam($noSkp);
    return "SELECT DISTINCT
              UNITKEY,
              NOSKP,
              '70' [STATUS],
              KEYBEND=ISNULL((SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].[BEND] b
              LEFT OUTER JOIN [V@LID49V6_2018_TES].[dbo].[PEMDA] p ON b.UNITKEY=p.CONFIGVAL
              WHERE JNS_BEND='01' AND CONFIGID='cur_uskpkd'),''),
              NPWPD,
              '1' IDXKODE,
              TGLSKP,
              '' PENYETOR,
              ALAMAT,
              URAISKP,
              TGLTEMPO,
              BUNGA,
              KENAIKAN,
              TGLVALID
              FROM TempSKP t
              WHERE TGLSKP BETWEEN '".$fromTgl."' AND '".$toTgl."' and NOSKP IN (".$skpParam.")";
  }

  public static function GetTempSkpForSKPDET($fromTgl, $toTgl, $noSkp) {
    $skpParam = self::stsParam($noSkp);
    return "SELECT
              MTGKEY=ISNULL((SELECT MTGKEY FROM [V@LID49V6_2018_TES].[dbo].[MATANGD] m WHERE t.KODE_REK=m.KDPER),''),
              '11' NOJETRA,
              UNITKEY,
              NOSKP,
              NILAI
              FROM TempSKP t
              WHERE t.TGLSKP BETWEEN '".$fromTgl."' AND '".$toTgl."' AND NOSKP IN (".$skpParam.")";
  }
  public static function stsParam(array $stsArray) {
    $stringData = "";
    for($i=0; $i < count($stsArray); $i++){
      if($i === count($stsArray)-1){
        $stringData .= "'".$stsArray[$i]."'";
      } else {
        $stringData .= "'".$stsArray[$i]."',";
      }
    }
    return $stringData;
  }
}