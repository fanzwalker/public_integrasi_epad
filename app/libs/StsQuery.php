<?php

namespace Integrasi\libs;

class StsQuery{
  public static function GetSts($fromTgl, $toTgl) {
    return "SELECT NoSTS,
                s1.TglSetoran,
                ISNULL(KodeUPT,(SELECT KodeUPT from MsUPT ms
                      LEFT OUTER JOIN PEMDA p ON replace(p.CONFIGVAL,'_','')=ms.UPTID
                WHERE p.CONFIGID='cur_uskpkd')) KodeUPT,
                ISNULL(UPT,(SELECT ms.UPT from MsUPT ms
                LEFT OUTER JOIN PEMDA p ON replace(p.CONFIGVAL,'_','')=ms.UPTID
                WHERE p.CONFIGID='cur_uskpkd')) UPT,
                m2.KodeRekening,
                m3.NMPER Rekening,
                m5.NIP,
                RTRIM(m5.NIP)+'-'+RTRIM(m5.Nama) Bendahara,
                s1.Keterangan,
                s2.JmlSetoran
                FROM SetoranUPTHdr s1
                LEFT OUTER JOIN MsUPT m1 on s1.UPTID=m1.UPTID
                LEFT OUTER JOIN SetoranUPTDtl s2 on s1.HeaderID=s2.HeaderID
                LEFT OUTER JOIN MsJenisPendapatan m2 on m2.JenisPendapatanID=s2.JenisPendapatanID
                LEFT OUTER JOIN V@LID49V6_2018_TES.dbo.MATANGD m3 on left(m2.KodeRekening,12)=left(m3.KDPER,12)
                LEFT OUTER JOIN  MsBendahara m4 ON s1.BendaharaID=m4.BendaharaID
                LEFT OUTER JOIN MsPegawai m5 ON m4.PegawaiID=m5.PegawaiID
                WHERE  s1.TglSetoran >= '".$fromTgl."' and s1.TglSetoran <= '".$toTgl."'";
  }

  public static function GetTempStsForSikpd($fromTgl, $toTgl, $noSts) {
    $stsParam = self::stsParam($noSts);
    return "SELECT DISTINCT
                        UNITKEY=ISNULL((SELECT TOP 1 UNITKEY FROM [V@LID49V6_2018_TES].[dbo].DAFTUNIT d WHERE d.KDUNIT=s.KodeUPT),
                            (SELECT CONFIGVAL FROM [V@LID49V6_2018_TES].[dbo].PEMDA p WHERE p.CONFIGID='cur_uskpkd')),
                        NoSTS,
                        KEYBEND1=ISNULL((SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].BEND b WHERE JNS_BEND='01'
                              AND UNITKEY IN(SELECT TOP 1 UNITKEY FROM [V@LID49V6_2018_TES].[dbo].DAFTUNIT d WHERE d.KDUNIT=s.KodeUPT)),
                              (SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].BEND b WHERE JNS_BEND='01'
                              AND UNITKEY IN(SELECT CONFIGVAL FROM [V@LID49V6_2018_TES].[dbo].PEMDA p WHERE p.CONFIGID='cur_uskpkd'))),
                        '16' as KDSTATUS,
                        '1' as IDXKODE,
                        KEYBEND2=ISNULL((SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].BEND b WHERE JNS_BEND='01'
                              AND UNITKEY IN(SELECT TOP 1 UNITKEY FROM [V@LID49V6_2018_TES].[dbo].DAFTUNIT d WHERE d.KDUNIT=s.KodeUPT)),
                              (SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].BEND b WHERE JNS_BEND='01'
                              AND UNITKEY IN(SELECT CONFIGVAL FROM [V@LID49V6_2018_TES].[dbo].PEMDA p WHERE p.CONFIGID='cur_uskpkd'))),
                        IDXTTD='',
                        NOBBANTU='',
                        s.TglSetoran as TGLSTS,
                        s.Keterangan,
                        s.TglSetoran as TGLVALID
                        FROM TempSTS s
                          where s.TglSetoran between '".$fromTgl."' and '".$toTgl."' and NoSTS IN (".$stsParam.")";
  }

  public static function GetTempStsForRKMDETD($fromTgl, $toTgl, $noSts) {
    $stsParam = self::stsParam($noSts);
    return "SELECT DISTINCT
              MTGKEY, UNITKEY, NOSTS, NOJETRA, NILAI
              FROM
              (
                SELECT
              MTGKEY=ISNULL((SELECT MTGKEY FROM [V@LID49V6_2018_TES].[dbo].[MATANGD] md WHERE RTRIM(md.KDPER)=LEFT(s.KodeRekening,12)),''),
              UNITKEY=ISNULL((SELECT TOP 1 UNITKEY FROM [V@LID49V6_2018_TES].dbo.DAFTUNIT d WHERE d.KDUNIT=s.KodeUPT),
                (SELECT CONFIGVAL FROM [V@LID49V6_2018_TES].[dbo].PEMDA p WHERE p.CONFIGID='cur_uskpkd')),
              NoSTS,
              '11' NOJETRA,
              s.JmlSetoran NILAI
              FROM TempSTS s
                WHERE s.TglSetoran between '".$fromTgl."' and '".$toTgl."' and NoSTS IN (".$stsParam.")
              )X
              WHERE X.MTGKEY<>''";
  }
  public static function getTempForSTSTBP($fromTgl, $toTgl, $noSts){
    $stsParam = self::stsParam($noSts);
    return "SELECT DISTINCT NOTBP,UNITKEY,NoSTS
              FROM
              (
                  SELECT RTRIM(NOTBP)+'/'+RTRIM(NOSKP) NOTBP,UNITKEY,s1.NoSTS,t.TGLTBP FROM tempTBP t
                  LEFT OUTER JOIN [VALIDMD].dbo.SetoranHist s ON t.NOTBP=s.NoSSPD AND t.NOSKP=s.SubKohir
                  LEFT OUTER JOIN [VALIDMD].dbo.SetoranUPTHdr s1 ON s.STS=s1.HeaderID
                  WHERE t.TGLTBP between '".$fromTgl."' and '".$toTgl."' and s1.NoSTS IN (".$stsParam.")
              )x
              WHERE x.NOTBP IS NOT NULL AND x.UNITKEY IS NOT NULL AND x.NoSTS IS NOT NULL
              ";
  }
  public static function stsParam(array $stsArray) {
    $stringData = "";
    for($i=0; $i < count($stsArray); $i++){
      if($i === count($stsArray)-1){
        $stringData .= "'".$stsArray[$i]."'";
      } else {
        $stringData .= "'".$stsArray[$i]."',";
      }
    }
    return $stringData;
  }
}