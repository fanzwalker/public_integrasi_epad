<?php
/**
 * Created by PhpStorm.
 * User: IF-PC
 * Date: 12/10/2017
 * Time: 1:35 PM
 */

namespace Integrasi\libs;


class TbpQuery
{
  public static function GetTbp($fromTgl, $toTgl){
    return "SELECT *
              FROM
              (
              SELECT RTRIM(s.NoSSPD) NOTBP,
                  RTRIM(s.SubKohir) NOSKP,
                  UNITKEY=REPLACE(ISNULL((SELECT RTRIM(p.CONFIGVAL) + '_' FROM PEMDA p WHERE p.CONFIGID='cur_uskpkd'),''), '__', '_'), 
                  mp.ObyekBadanNo NPWPD, 
                  s.TglBayar TGLTBP,
                  s.NamaPenyetor PENYETOR, 
                  mp.AlamatBadan ALAMAT, 
                  s.Keterangan URAITBP,
                  CAST(0 AS MONEY) BUNGA,
                  CAST(0 AS MONEY) KENAIKAN,
                  hd.TglJatuhTempo TGLVALID,
                  mj.KodeRekening KODE_REK,
                  mj.NamaJenisPendapatan REKENING,
                  dt.TarifPajak NILAI
                  FROM SetoranHist s
                  INNER JOIN PenetapanReklameHdr hd ON s.SubKohir=hd.KohirID and s.NoKohir=hd.NoKohir
                  LEFT OUTER JOIN PenetapanReklameDtl dt ON hd.NoKohir=dt.NoKohir
                  LEFT OUTER JOIN MsWP ms ON hd.ObyekBadanID=ms.ObyekBadanID
                  LEFT OUTER JOIN MsWPData mp ON ms.WPID=mp.WPID
                  LEFT OUTER JOIN MsJenisPendapatan mj ON ms.UsahaBadan=mj.JenisPendapatanID
                  UNION
                  SELECT RTRIM(s.NoSSPD) NOTBP,
                  RTRIM(s.SubKohir) NOSKP,
                  UNITKEY=REPLACE(ISNULL((SELECT RTRIM(p.CONFIGVAL) + '_' FROM PEMDA p WHERE p.CONFIGID='cur_uskpkd'),''),'__','_'),
                  p.ObyekBadanNo NPWPD,
                  s.TglBayar TGLTBP,
                  s.NamaPenyetor PENYETOR,
                  p.AlamatBadan ALAMAT,
                  p.Jenis URAITBP,
                  BUNGA=ISNULL((CASE WHEN Pajak=0 THEN 0 ELSE (JDenda/Pajak) END),0),
                  JDenda KENAIKAN,
                  p.TglJatuhTempo TGLVALID,
                  p.KodeRekening KODE_REK,
                  p.Jenis REKENING,
                  p.Pajak NILAI
                  FROM SetoranHist s 
                  INNER JOIN vw_penetapanomzet p ON s.SubKohir=p.KohirID and s.NoKohir=p.NoKohir 
                  )x
                  WHERE x.NOTBP<>'' AND x.TGLTBP BETWEEN '".$fromTgl."' AND '".$toTgl."'";
  }
  public static function GetTempTbpForSIKPD($fromTgl, $toTgl, $noTbp) {
    $tbpParam = self::tbpParam($noTbp);
    return "SELECT DISTINCT
              UNITKEY,
              RTRIM(NOTBP)+'/'+RTRIM(NOSKP) NOTBP,
              KEYBEND1=ISNULL((SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].[BEND] b
              LEFT OUTER JOIN [V@LID49V6_2018_TES].[dbo].[PEMDA] p ON b.UNITKEY=p.CONFIGVAL
              WHERE JNS_BEND='01' AND CONFIGID='cur_uskpkd'),''),
              '61' KDSTATUS,
              KEYBEND2=ISNULL((SELECT TOP 1 KEYBEND FROM [V@LID49V6_2018_TES].[dbo].[BEND] b
              LEFT OUTER JOIN [V@LID49V6_2018_TES].[dbo].[PEMDA] p ON b.UNITKEY=p.CONFIGVAL
              WHERE JNS_BEND='01' AND CONFIGID='cur_uskpkd'),''),
              '1' IDXKODE,
              TGLTBP,
              PENYETOR,
              ALAMAT,
              URAITBP=STUFF(ISNULL((SELECT ', ' + RTRIM(x.URAITBP)
                        FROM TempTBP x
                        WHERE x.NOTBP = t.NOTBP
                    GROUP BY x.UNITKEY,PENYETOR,NOTBP,URAITBP
                        FOR XML PATH (''), TYPE).value('.','VARCHAR(max)'), ''), 1, 2, ''),
              TGLVALID
              FROM TempTBP t
              WHERE TGLTBP BETWEEN '".$fromTgl."' AND '".$toTgl."' and NOTBP IN (".$tbpParam.")";
  }

  public static function GetTempTbpForTBPDET($fromTgl, $toTgl, $noTbp) {
    $tbpParam = self::tbpParam($noTbp);
    return "SELECT 
              MTGKEY=ISNULL((SELECT MTGKEY FROM [V@LID49V6_2018_TES].[dbo].[MATANGD] m WHERE t.KODE_REK=m.KDPER),''),
              '11' NOJETRA,
              UNITKEY,
              RTRIM(NOTBP)+'/'+RTRIM(NOSKP) NOTBP,
              NILAI
              FROM TempTBP t
              WHERE t.TGLTBP BETWEEN '".$fromTgl."' AND '".$toTgl."' AND NOTBP IN (".$tbpParam.")";
  }
  public static function getTempForSKPTBP($fromTgl, $toTgl, $noTbp){
    $tbpParam = self::tbpParam($noTbp);
    return "SELECT DISTINCT RTRIM(NOTBP)+'/'+RTRIM(NOSKP) NOTBP,UNITKEY,NOSKP FROM TempTBP
      WHERE TGLTBP BETWEEN '".$fromTgl."' AND '".$toTgl."' AND NOTBP IN (".$tbpParam.")";
  }
  public static function tbpParam(array $stsArray) {
    $stringData = "";
    for($i=0; $i < count($stsArray); $i++){
      if($i === count($stsArray)-1){
        $stringData .= "'".$stsArray[$i]."'";
      } else {
        $stringData .= "'".$stsArray[$i]."',";
      }
    }
    return $stringData;
  }
}