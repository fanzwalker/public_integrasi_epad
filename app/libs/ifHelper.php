<?php

namespace Integrasi\libs;

class ifHelper {
  public static function splitTgl($str_tgl) {
    if(strlen($str_tgl) > 0):
      $pecah = explode('-', $str_tgl);
      return $pecah[2]."-".$pecah[1]."-".$pecah[0];
    endif;
  }
  public static function SipkdName($tahun) {
    return 'V@LID49V6_'.$tahun.'_TES';
  }
}