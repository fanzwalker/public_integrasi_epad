<?php

namespace App\models\InStage;

use Illuminate\Database\Eloquent\Model;

class MATANGD extends Model
{
  protected $table = 'MATANGD';
  protected $fillable = ['MTGKEY','MTGLEVEL','KDPER','NMPER','TYPE','MAT_MTGKEY'];
  public $timestamps = false;
}
