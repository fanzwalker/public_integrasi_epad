<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Home</title>
  <!-- Styles -->
  <link href="{{ asset('css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/css/home.css') }}">
</head>
<body>
  <div class="container-fluid">
    <div class="row text-center">
      <div class="col-md-8 col-md-offset-2">
        <div class="panel panel-default panelMain">
          <div class="panel-heading">
            Menu Integrasi Data
          </div>
          <div class="panel-body">
            <div class="col-md-4">
              <div class="panel panel-primary animated bounceIn">
                <div class="panel-heading">
                  SKP
                </div>
                <div class="panel-body">
                  Surat Ketetapan Pajak
                </div>
                <div class="panel-footer">
                  <a href="{{ route('skp.index') }}" class="btn btn-primary btn-block">
                    <i class="glyphicon glyphicon-copy"></i>&nbsp;
                    Integrasi
                  </a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-primary animated bounceIn">
                <div class="panel-heading">
                  TBP
                </div>
                <div class="panel-body">
                  Tanda Bukti Pembayaran
                </div>
                <div class="panel-footer">
                  <a href="{{ route('tbp.index') }}" class="btn btn-primary btn-block">
                    <i class="glyphicon glyphicon-copy"></i>&nbsp;
                    Integrasi
                  </a>
                </div>
              </div>
            </div>
            <div class="col-md-4">
              <div class="panel panel-primary animated bounceIn">
                <div class="panel-heading">
                  STS
                </div>
                <div class="panel-body">
                  Surat Tanda Setoran
                </div>
                <div class="panel-footer">
                  <a href="{{ route('sts.index') }}" class="btn btn-primary btn-block">
                    <i class="glyphicon glyphicon-copy"></i>&nbsp;
                    Integrasi
                  </a>
                </div>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <a href="{{ route('logout') }}" class="btn btn-danger"
               onclick="event.preventDefault();
               document.getElementById('logout-form').submit();">
              <i class="glyphicon glyphicon-log-out"></i>&nbsp;
              Keluar
            </a>
            <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
              {{ csrf_field() }}
            </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>
