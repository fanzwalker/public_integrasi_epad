<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>{{ config('app.name', 'Laravel') }}</title>

    <!-- Styles -->
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    <link rel="stylesheet" href="{{ asset('vendor/css/main.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/loading/loading.min.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/animate/animate.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/datepicker/dist/css/bootstrap-datepicker.css') }}">
    <link rel="stylesheet" href="{{ asset('vendor/sweetalert/dist/sweetalert.css') }}">
    <link href="{{ asset('vendor/footable/css/footable.standalone.css') }}" rel="stylesheet">
    <link href="{{ asset('vendor/css/footable.override.css') }}" rel="stylesheet">
    @yield('styles')
</head>
<body>
    <div id="app">
        <nav class="navbar navbar-default navbar-static-top">
            <div class="container">
                <div class="navbar-header">

                    <!-- Collapsed Hamburger -->
                    <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#app-navbar-collapse" aria-expanded="false">
                        <span class="sr-only">Toggle Navigation</span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                        <span class="icon-bar"></span>
                    </button>

                    <!-- Branding Image -->
                    <a class="navbar-brand" href="{{ route('home') }}">
                        {{ config('app.name', 'APP_NAME') }}
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="app-navbar-collapse">
                    <!-- Left Side Of Navbar -->
                    <ul class="nav navbar-nav">
                      <li><a href="{{ route('skp.index') }}">SKP</a></li>
                      <li><a href="{{ route('tbp.index') }}">TBP</a></li>
                      <li><a href="{{ route('sts.index') }}">STS</a></li>
                    </ul>

                    <!-- Right Side Of Navbar -->
                    <ul class="nav navbar-nav navbar-right">
                        <!-- Authentication Links -->
                        @guest
                            <li><a href="{{ route('login') }}">Login</a></li>
                            <li><a href="{{ route('register') }}">Register</a></li>
                        @else
                        <li><a href="#">Database V@LID49V6_{{ Session::get('tahun') }}</a></li>
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false" aria-haspopup="true">
                                    {{ Auth::user()->username }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li>
                                        <a href="{{ route('logout') }}"
                                            onclick="event.preventDefault();
                                                     document.getElementById('logout-form').submit();">
                                            Logout
                                        </a>

                                        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
                                            {{ csrf_field() }}
                                        </form>
                                    </li>
                                </ul>
                            </li>
                        @endguest
                    </ul>
                </div>
            </div>
        </nav>

        @yield('content')
    </div>

    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}"></script>
    <script src="{{ asset('vendor/loading/jquery.loading.min.js') }}"></script>
    <script src="{{ asset('vendor/datepicker/dist/js/bootstrap-datepicker.js') }}"></script>
    <script src="{{ asset('vendor/datepicker/dist/locales/bootstrap-datepicker.id.min.js') }}"></script>
    <script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
    <script src="{{ asset('vendor/footable/js/footable.js') }}"></script>
    <script src="{{ asset('vendor/footable/js/footable.paginate.js') }}"></script>
    <script src="{{ asset('vendor/footable/js/footable.sort.js') }}"></script>
    <script src="{{ asset('vendor/footable/js/footable.filter.js') }}"></script>
    <script>
      var url = window.location;
      var TahunDB = "{{ Session::get('tahun') }}";
      // Will only work if string in href matches with location
      $('ul a[href="' + url + '"]').parent().addClass('active');

      // Will also work for relative and absolute hrefs
      $('ul a').filter(function () {
        return this.href == url;
      }).parent().addClass('active').parent().parent().addClass('active');

      function rupiah(value)
      {
        if(value) {
          value += '';
          x = value.split('.');
          x1 = x[0];
          x2 = x.length > 1 ? '.' + x[1] : '';
          var rgx = /(\d+)(\d{3})/;
          while (rgx.test(x1)) {
            x1 = x1.replace(rgx, '$1' + '.' + '$2');
          }
          return 'Rp. ' + x1 + x2 + ",00";
        } else {
          return 'Rp. 0,00';
        }
      };
      function CalculateDays(startDate, endDate) {
        var startTgl = new Date(reFormateDate(startDate));
        var endTgl =  new Date(reFormateDate(endDate));
        return (endTgl- startTgl) / (1000 * 60 * 60 * 24);
      }
      function CheckTahun(dataDate) {
        var tempData = dataDate.split('-');
        if(tempData[2] === TahunDB) {
          return true;
        }
        return false;
      }
      function reFormateDate(dataDate){
        var tempDate = dataDate.split('-');
        return tempDate[1]+'-'+tempDate[0]+'-'+tempDate[2];
      }
    </script>
    @yield('scripts')
</body>
</html>
