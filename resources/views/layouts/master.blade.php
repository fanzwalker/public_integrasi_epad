<!DOCTYPE html>
<html lang="{{ app()->getLocale() }}">
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1">

  <!-- CSRF Token -->
  <meta name="csrf-token" content="{{ csrf_token() }}">

  {{--<title>{{ config('app.name', 'Laravel') }}</title>--}}
  <title>Integerasi Data</title>
  <!-- Styles -->
  <link href="{{ asset('vendor/css/app.css') }}" rel="stylesheet">
  <link rel="stylesheet" href="{{ asset('vendor/css/override.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/css/loading.min.css') }}">
  {{--datatable--}}
  <link rel="stylesheet" href="{{ asset('vendor/dataTable/css/dataTables.bootstrap4.min.css') }}">
  <link rel="stylesheet" href="{{ asset('vendor/sweetalert/dist/sweetalert.css') }}">
  {{--footable--}}
  {{--<link rel="stylesheet" href="{{ asset('vendor/footable-components.latest/css/footable.core.bootstrap.css') }}">--}}
  {{--<link rel="stylesheet" href="{{ asset('vendor/footable-components.latest/css/footable.paging.css') }}">--}}
  {{--<link rel="stylesheet" href="{{ asset('vendor/footable-components.latest/css/footable.filtering.css') }}">--}}
  @yield('styles')
</head>
<body>
  @include('layouts.navbar')
  <div class="container-fluid">
    @yield('content')
  </div>
<!-- Scripts -->
<script src="{{ asset('vendor/js/app.js') }}"></script>
<script src="{{ asset('vendor/js/jquery.loading.min.js') }}"></script>
  {{--dataTable--}}
  <script src="{{ asset('vendor/dataTable/js/jquery.dataTables.min.js') }}"></script>
  <script src="{{ asset('vendor/dataTable/js/dataTables.bootstrap4.min.js') }}"></script>
  <script src="{{ asset('vendor/sweetalert/dist/sweetalert.min.js') }}"></script>
  {{--footable--}}
  {{--<script src="{{ asset('vendor/footable-components.latest/js/footable.core.js') }}"></script>--}}
  {{--<script src="{{ asset('vendor/footable-components.latest/js/footable.paging.js') }}"></script>--}}
  {{--<script src="{{ asset('vendor/footable-components.latest/js/footable.filtering.js') }}"></script>--}}
  @yield('scripts')
</body>
</html>