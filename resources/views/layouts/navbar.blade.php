<nav class="navbar navbar-expand-lg navbar-light bg-light">
  <a class="navbar-brand" href="#">IntegApp</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>

  <div class="collapse navbar-collapse" id="navbarSupportedContent">
    <ul class="navbar-nav mr-auto">
      @auth
        {{--<li class="nav-item dropdown">--}}
          {{--<a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">--}}
            {{--Integrasi Data--}}
          {{--</a>--}}
          {{--<div class="dropdown-menu dropdown_kiri" aria-labelledby="navbarDropdownMenuLink">--}}
            {{--<a class="dropdown-item" href="{{ route('person') }}">Sample Person</a>--}}
            {{--<a class="dropdown-item" href="{{ route('pendapatan') }}">Tabel Pendapatan</a>--}}
            {{--<a class="dropdown-item" href="{{ route('sts.index') }}">STS</a>--}}
          {{--</div>--}}
        {{--</li>--}}
        <li class="nav-item" style="padding-right: 10px">
          <a class="btn btn-outline-primary" href="{{ route('sts.index') }}">
            <i class="fa fa-file-archive-o"></i>&nbsp;STS</a>
        </li>
        <li class="nav-item" style="padding-right: 10px">
          <a class="btn btn-outline-primary" href="{{ route('skp.index') }}">
            <i class="fa fa-file-archive-o"></i>&nbsp;SKP</a>
        </li>
      @endauth
    </ul>
    <ul class="navbar-nav justify-content-end">
      @guest
        <li class="nav-item">
          <a class="nav-link" href="{{ route('login') }}">Login</a>
        </li>
      @else
        <li class="nav-item dropdown">
          <a class="nav-link dropdown-toggle" href="#" id="navbarDropdownMenuLink" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
            {{ Auth::user()->username }}
          </a>
          <div class="dropdown-menu dropdown_kanan" aria-labelledby="navbarDropdownMenuLink">
            <a class="nav-link text-center" href="{{ route('logout') }}" onclick="event.preventDefault();
                                                   document.getElementById('logout-form').submit();">Logout</a>
          </div>
        </li>
        <form id="logout-form" action="{{ route('logout') }}" method="POST" style="display: none;">
          {{ csrf_field() }}
        </form>
      @endguest
    </ul>
  </div>
</nav>