@extends('layouts.master')
@section('styles')
  {{--<style rel="stylesheet">--}}
    {{--.card_preview{--}}
      {{--max-height: 600px !important;--}}
      {{--overflow-y: scroll;--}}
    {{--}--}}
  {{--</style>--}}
@endsection
@section('content')
  <div class="row justify-content-center">
    <div class="col-12">
      <div class="card">
        <div class="card-header text-center">
          Intergrasi Tabel Pendapatan
        </div>
        <div class="card-body">
          <div class="row">
            <div class="col-12 text-center">
              <button type="button" class="btn btn-success" id="btn_get">
                Get Data Pendapatan
              </button>
            </div>
          </div>
          <div class="row">
            <div class="col-12">
              <table class="table table-bordered table-responsive tabel_Pendapatan" cellspacing="0" width="100%">
                <thead>
                  <tr>
                    <th>KDPER</th>
                    <th>NMPER</th>
                    <th>TYPE</th>
                  </tr>
                </thead>
                <tbody>

                </tbody>
              </table>
              <div id="paging-ui-container"></div>
            </div>
          </div>
        </div>
        <div class="card-footer">
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script>
    var dataForStore;
//    var tbl = $('.tabel_Pendapatan').DataTable();
    $(function(){
//      $('.tabel_Pendapatan').DataTable();
//      $('.tabel_Pendapatan').footable();
    });
    $(document).on('click','#btn_get', function(){
      $.showLoading();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : "{{ route('get_from') }}",
        type: "POST",
        dataType: "JSON",
        success: function(response){
          $.hideLoading();
          if(response.length > 0) {
            dataForStore = response;
            var Body = "";
            $.each(dataForStore, function(i){
              Body += "<tr>" +
                "<td>"+ dataForStore[i].KDPER +"</td>" +
                "<td>"+ dataForStore[i].NMPER +"</td>" +
                "<td>"+ dataForStore[i].TYPE +"</td>" +
                "</tr>";
            });
            console.log(Body);
            $('.tabel_Pendapatan > tbody').empty();
            $('.tabel_Pendapatan > tbody').append(Body);
            $('.tabel_Pendapatan').DataTable();
          } else {
            $('.tabel_Pendapatan > tbody').empty();
          }

        }
      });
    });
    $(document).on('click','#btn_store', function(){
      console.log(dataForStore);
    });
  </script>
@endsection