@extends('layouts.app')

@section('content')
  <div class="container-fluid">
    <div class="row">
      <div class="col-md-12">
        <div class="panel panel-primary">
          <div class="panel-heading">
            <div class="row">
              <div class="col-md-6">
                <form class="form-inline">
                  <div class="form-group form-group-sm">
                    <label for="fromTgl">Tanggal Penetapan</label>
                    <input type="text" class="form-control datepicker-fromTgl" id="fromTgl" name="fromTgl">
                  </div>
                  <div class="form-group form-group-sm">
                    <label for="toTgl">s/d</label>
                    <input type="text" class="form-control datepicker-toTgl" id="toTgl" name="toTgl">
                  </div>
                  <button type="button" class="btn btn-default btn-sm" id="btn_get">
                    <i class="glyphicon glyphicon-save"></i>&nbsp;Ambil Data</button>
                </form>
              </div>
            </div>
          </div>
          <div class="panel-body">
            <div class="row">
              <div class="col-md-3 col-sm-12 col-xs-12">
                <form class="form-inline" style="margin-bottom:10px;">
                  <div class="form-group">
                    <label for="filter">Cari Data</label>
                    <input type="text" class="form-control input-sm" id="filter" placeholder="Cari Data">
                  </div>
                </form>
              </div>
              <div class="col-md-9 col-sm-12 col-xs-12 place_btn_store">

              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <table class="table table-bordered table-responsive tabel_skp" id="tabel_skp" data-filter="#filter" data-page-navigation=".pagination"  data-limit-navigation="5" data-toggle="true"
                       data-page-size="10" data-paging="true" data-sort="false">
                  <thead>
                  <tr>
                    <th style="text-align: center; width: 45px"><input type="checkbox" class="checkAll"></th>
                    <th>No SKP</th>
                    <th>NPWPD</th>
                    <th data-hide="phone,tablet">Tgl SKP</th>
                    <th data-hide="phone,tablet">Penyetor</th>
                    <th data-hide="phone,tablet">Alamat</th>
                    <th data-hide="phone,tablet">Uraian SKP</th>
                    <th data-hide="phone,tablet">Tgl Tempo</th>
                    <th data-hide="phone,tablet">Bunga</th>
                    <th data-hide="phone,tablet">Kenaikan</th>
                    <th data-hide="phone,tablet">Status</th>
                    <th data-hide="phone,tablet">Tgl VALID</th>
                    <th data-hide="phone,tablet">Kode Rek</th>
                    <th data-hide="phone,tablet">Rekening</th>
                    <th data-hide="phone,tablet">Nilai</th>
                  </tr>
                  </thead>
                  <tbody>

                  </tbody>
                  <tfoot class="">
                  <tr>
                    <td colspan="15" class="text-center">
                      <div class="pagination pagination-centered hide-if-no-paging"></div>
                    </td>
                  </tr>
                  </tfoot>
                </table>
              </div>
            </div>
          </div>
          <div class="panel-footer">
            <div class="row">
              <div class="col-md-4">
                <label>Jumlah Data : <span class="jumlahData"></span></label>
              </div>
            </div>
          </div>
        </div>
      </div>
    </div>
  </div>
@endsection
@section('scripts')
  <script>
    var dataForStore;
    var fromTgl;
    var toTgl;
    $(function(){
      $('.tabel_skp').footable({
        "filtering": {
          "enabled": true
        }
      });
      $('.datepicker-fromTgl, .datepicker-toTgl').datepicker({
        language: "id",
        format: "dd-mm-yyyy",
      });
      $('span.jumlahData').text(0);
      getFromPend();
    });
    $(document).on('change', '.checkAll', function(){
      $('.checkItem').prop('checked', $(this).prop('checked'));
    });
    $(document).on('click','#btn_get', function(){
      fromTgl = $("#fromTgl").val();
      toTgl = $("#toTgl").val();
      getFromPend();
    });
    function getFromPend(){
      var paramPost = {
        fromTgl: fromTgl,
        toTgl: toTgl
      };
      if(paramPost.fromTgl && paramPost.toTgl){
        var countHari = CalculateDays(paramPost.fromTgl, paramPost.toTgl);
        if(countHari > 31) {
          swal({
            html: true,
            title: "Info",
            text: 'Maksimal 1 Bulan / 31 hari',
            type: 'info'
          });
        } else {
          if(CheckTahun(paramPost.fromTgl)) {
            $.showLoading();
            $.ajax({
              headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
              },
              url : "{{ route('skp.get') }}",
              type: "POST",
              data: paramPost,
              async : true,
              success: function(resp) {
                if(resp.status){
                  getFromStage();
                } else {
                  $.hideLoading();
                  swal({
                    html : true,
                    title: "Info",
                    text: resp.info,
                    type: 'warning'
                  }, function(){
                    return false;
                  });
                }
              }
            });
          } else { 
          swal({
              html: true,
              title: "Info",
              text: 'Tahun tidak sesuai',
              type: 'info'
            });
          } 
        }
      }
    }
    function getFromStage() {
      var paramPost = {
        fromTgl: fromTgl,
        toTgl: toTgl
      };
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url : "{{ route('skp.getfromStage') }}",
        type: "POST",
        data: paramPost,
        dataType: "JSON",
        async : true,
        success: function(response){
          $.hideLoading();
          if(response.TempSKP.length > 0) {
            $('span.jumlahData').text(response.TempSKP.length);
            $('.place_btn_store').find('button#btn_store').remove();
            $('.place_btn_store').append('<button type="button" class="btn btn-success btn-sm" style="float: right" id="btn_store">' +
              '<i class="glyphicon glyphicon-floppy-disk"></i>&nbsp; Simpan ke SIPKD</button>');
            dataForStore = response.TempSKP;
            var Body = "";
            var Bg = "";
            var fontColor = "";
            var disabledCheck = "";
            var valueCheckBox = "";
            $.each(dataForStore, function(i){
              if(response.LogSKP.length > 0){
                $.each(response.LogSKP, function(j){
                  if(dataForStore[i].NOSKP === response.LogSKP[j].NOSKP){
                    Bg = "#880000";
                    fontColor = "#FFFFFF";
                    disabledCheck = "disabled";
                    valueCheckBox = "";
                    return false;
                  } else {
                    Bg = "";
                    fontColor = "";
                    disabledCheck = "";
                    valueCheckBox = dataForStore[i].NOSKP;
                    return true;
                  }
                });
              } else {
                valueCheckBox = dataForStore[i].NOSKP;
              }
              Body += "<tr style='background-color: " + Bg + "; color: " + fontColor + "'>" +
                "<td style='text-align: center'><input type='checkbox' class='checkItem' " + disabledCheck + " value='"+ valueCheckBox +"'/> </td>" +
                "<td>"+ dataForStore[i].NOSKP +"</td>" +
                "<td>"+ dataForStore[i].NPWPD +"</td>" +
                "<td>"+ dataForStore[i].TGLSKP +"</td>" +
                "<td>"+ dataForStore[i].PENYETOR +"</td>" +
                "<td>"+ dataForStore[i].ALAMAT +"</td>" +
                "<td>"+ dataForStore[i].URAISKP +"</td>" +
                "<td>"+ dataForStore[i].TGLTEMPO +"</td>" +
                "<td>"+ dataForStore[i].BUNGA +"</td>" +
                "<td>"+ dataForStore[i].KENAIKAN +"</td>" +
                "<td>"+ dataForStore[i].STATUS +"</td>" +
                "<td>"+ dataForStore[i].TGLVALID +"</td>" +
                "<td>"+ dataForStore[i].KODE_REK +"</td>" +
                "<td>"+ dataForStore[i].REKENING +"</td>" +
                "<td>"+ rupiah(parseFloat(parseFloat(dataForStore[i].NILAI).toFixed(2))) +"</td>" +
                "</tr>";
            });
            $('.tabel_skp > tbody').empty();
            $('.tabel_skp > tbody').append(Body).trigger('footable_redraw');
          } else {
            $('.place_btn_store').find('button#btn_store').remove();
            $('.tabel_skp > tbody').empty();
            $('.tabel_skp > tbody').trigger('footable_redraw');
          }
        }
      });
    }
    $(document).on('click','#btn_store', function(){
      var dataCheck = $('.checkItem:checked').map(function(){
        if($(this).val() !== "") {
          return $(this).val();
        }
      }).get();
      var dataSKP = $.unique(dataCheck);
      var tempArray = new Array();
      for(var i = 0; i < dataSKP.length; i++){
        $.grep(dataForStore, function (e) {
          if(e.NoSKP === dataSKP[i]){
            tempArray.push(e);
          }
        });
      }
      var fromTgl = $("#fromTgl").val();
      var toTgl = $("#toTgl").val();
      var paramPost = {
        fromTgl: fromTgl,
        toTgl: toTgl,
        noSKP: dataSKP
      };
      $.showLoading();
      $.ajax({
        headers: {
          'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        },
        url: "{{ route('skp.store') }}",
        type: "POST",
        data: paramPost,
        dataType: "JSON",
        success: function (response) {
          $.hideLoading();
          if(response.status === "success") {
            swal({
              html : true,
              title: "Berhasil",
              text: response.info,
              type: response.status
            }, function(){
              getFromStage();
            });
          } else {
            swal({
              html : true,
              title: "Gagal",
              text: response.info,
              type: response.status
            }, function(){
              getFromStage();
            });
          }
        },
        complete: function(xhr) {
          $.hideLoading();
          console.log(xhr.status);
          if(xhr.status === 500) {
            swal({
              html : true,
              title: "Gagal",
              text: "Terjadi Kesalahan",
              type: "warning"
            });
            console.log(xhr.responseJSON.message);
          }
        }
      });
    });
  </script>
@endsection