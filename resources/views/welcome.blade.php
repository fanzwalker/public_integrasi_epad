<!doctype html>
<html lang="{{ app()->getLocale() }}">
    <head>
      <meta charset="utf-8">
      <meta http-equiv="X-UA-Compatible" content="IE=edge">
      <meta name="viewport" content="width=device-width, initial-scale=1">

      <title>Integrasi</title>

      <!-- Fonts -->
      <link href="https://fonts.googleapis.com/css?family=Raleway:100,600" rel="stylesheet" type="text/css">
      <!-- Styles -->
      <link href="{{ asset('css/app.css') }}" rel="stylesheet">
      <link href="{{ asset('vendor/css/login.css') }}" rel="stylesheet">
    </head>
    <body>
      <div class="container-fluid">
        <div class="row headArae">
          <div class="col-md-6">
            <div class="row">
              <div class="col-md-1 col-sm-2 col-xs-2" style="margin: 3px">
                <img src="{{ asset('./images/kapuas_logo.png') }}" class="img-responsive logo_p">
              </div>
              <div class="col-md-10 col-sm-12 title_p">
                <h2>Kabupaten Kapuas Hulu</h2>
              </div>
            </div>
          </div>
          <div class="col-md-6">
            <div class="panel panel-default panelLogin">
              <div class="panel-body">
                <form class="form" method="POST" action="{{ route('login') }}">
                  {{ csrf_field() }}
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group form-group-sm">
                      <label for="email">Username / Email</label>
                      <input type="text" class="form-control" id="email" name="email_or_username" placeholder="Username / Email" required>
                    </div>
                  </div>
                  <div class="col-md-4 col-sm-12">
                    <div class="form-group form-group-sm">
                      <label for="password">Password</label>
                      <input type="password" class="form-control" id="password" name="password" required placeholder="Password">
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-12">
                    <div class="form-group form-group-sm">
                      <label for="tahun">Tahun</label>
                      <select name="tahun" id="tahun" class="form-control">
                        @foreach($listDB as $list)
                          @if(strpos($list->DATABASE_NAME, 'V@LID49V6') !== false && strlen($list->DATABASE_NAME) === 14)
                            <option value="{{substr($list->DATABASE_NAME, 10, 4)}}">{{substr($list->DATABASE_NAME, 10, 4)}}</option>
                          @endif
                        @endforeach
                        {{--<option value="2017">2017</option>--}}
                        {{--<option value="2018" selected>2018</option>--}}
                      </select>
                    </div>
                  </div>
                  <div class="col-md-2 col-sm-12">
                    <div class="form-group form-group-sm">
                      <button type="submit" class="btn btn-success btn-block" style="margin-top: 23px">
                        <i class="glyphicon glyphicon-log-in"></i>&nbsp;
                        Masuk</button>
                    </div>
                  </div>
                </form>
              </div>
            </div>
          </div>
        </div>
        <div class="row">
          <div class="col-md-6 col-md-offset-3 hidden-sm" style="text-align: center; margin-top: 50px">
            <img src="{{ asset('./images/logo_i.png') }}" class="logo_i">
          </div>
        </div>
      </div>
    </body>
</html>
