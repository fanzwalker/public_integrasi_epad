<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Auth::routes();
Route::get('/', [
  'uses'  => 'SignController@index',
  'as'    => 'auth.signin'])->middleware('guest');
Route::get('login', [
  'uses'  => 'SignController@index',
  'as'    => 'login'
]);
Route::post('login', [
  'uses'  => 'SignController@signIn',
  'as'    => 'auth.signin'
]);
Route::get('home', [
  'uses'  => 'HomeController@index',
  'as'    => 'home'
])->middleware('auth');
Route::prefix('migrasi')->middleware('auth')->group(function(){
  Route::get('person', [
    'uses'  => 'SamplePersonController@index',
    'as'    => 'person'
  ]);
  Route::get('pendapatan', [
    'uses'  => 'MATANGDController@index',
    'as'    => 'pendapatan'
  ]);
  Route::post('get_from', [
    'uses'  => 'MATANGDController@getFrom',
    'as'    => 'get_from'
  ]);
  Route::prefix('sts')->group(function(){
    Route::get('/',[
      'uses'  => 'SuratTandaSetoranController@index',
      'as'    => 'sts.index'
    ]);
    Route::post('getSTS',[
      'uses'  => 'SuratTandaSetoranController@getFromPendapatan',
      'as'    => 'sts.get'
    ]);
    Route::post('getSTSFromStage',[
      'uses'  => 'SuratTandaSetoranController@getFromStage',
      'as'    => 'sts.getfromStage'
    ]);
    Route::post('StoreSTSToSIKPD',[
      'uses'  => 'SuratTandaSetoranController@storeSTSSikpd',
      'as'    => 'sts.store'
    ]);
  });
  Route::prefix('skp')->group(function(){
    Route::get('/', [
      'uses'  => 'SuratKetetapanPajakController@index',
      'as'    => 'skp.index'
    ]);
    Route::post('getSKP',[
      'uses'  => 'SuratKetetapanPajakController@getFromPendapatan',
      'as'    => 'skp.get'
    ]);
    Route::post('getSKPFromStage',[
      'uses'  => 'SuratKetetapanPajakController@getFromStage',
      'as'    => 'skp.getfromStage'
    ]);
    Route::post('StoreSKPToSIKPD',[
      'uses'  => 'SuratKetetapanPajakController@storeSKPSikpd',
      'as'    => 'skp.store'
    ]);
  });
  Route::prefix('tbp')->group(function(){
    Route::get('/', [
      'uses'  => 'TandaBuktiPembayaranController@index',
      'as'    => 'tbp.index'
    ]);
    Route::post('getTBP',[
      'uses'  => 'TandaBuktiPembayaranController@getFromPendapatan',
      'as'    => 'tbp.get'
    ]);
    Route::post('getTBPFromStage',[
      'uses'  => 'TandaBuktiPembayaranController@getFromStage',
      'as'    => 'tbp.getfromStage'
    ]);
    Route::post('StoreTBPToSIKPD',[
      'uses'  => 'TandaBuktiPembayaranController@storeTBPSikpd',
      'as'    => 'tbp.store'
    ]);
  });
  Route::prefix('settings')->group(function(){
    Route::post('resetLog', [
      'uses'  => 'SettingsController@ResetTableLog',
      'as'    => 'reset.log'
    ]);
  });
});